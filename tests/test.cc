#include "../sylar/log.h"
#include "../sylar/util.h"
#include <iostream>

int main(int argc, char** argv) {

    /*使用自定义日志器*/
    // Logger在构造函数时就指定了默认的m_formatter，如果后续给Logger添加appender时，如果appander没设置格式，则使用Logger默认的。正如此例
    sylar::Logger::ptr logger(new sylar::Logger); //new后直接是类型，不要加括号
    logger->addAppender(sylar::LogAppender::ptr(new sylar::StdoutLogAppender));

    sylar::FileLogAppender::ptr fileAppender(new sylar::FileLogAppender("./log.txt"));
    sylar::LogFormatter::ptr logFmt(new sylar::LogFormatter("%d%T%p%T%m%n"));
    fileAppender->setFormatter(logFmt);
    fileAppender->setLevel(sylar::LogLevel::ERROR); // 把它的级别设置的高一些
    logger->addAppender(fileAppender);

    // sylar::LogEvent::ptr event(new sylar::LogEvent(__FILE__, __LINE__, 0, sylar::getPid(), sylar::getFid(), time(0)));
    // event->getSS() << "hello log";
    // logger->log(sylar::LogLevel::DEBUG, event);
    // std::cout << "hello log" << std::endl;

    // SYLAR_LOG_LEVEL(logger, sylar::LogLevel::INFO) << "HELLO LOG";
    SYLAR_LOG_DEBUG(logger) << "HELLO SYLAR"; // 两个appender都会走这两个log，但是s的级别时默认的debug，可以通过这个输出
    SYLAR_LOG_FMT_ERROR(logger, "hello %s fmt log", "ERROR"); // f的级别高一些，会走这个

    /*使用内置的m_root日志器*/
    auto l = sylar::LogMgr::getInstance()->getLogger("xx"); // 没有名为xx的日志器，故使用m_root输出
    SYLAR_LOG_DEBUG(l) << "xx";



    return 0;
}
