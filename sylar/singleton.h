#ifndef __SYLAR_SINGLETON_H
#define __SYLAR_SINGLETON_H
#include <memory>

// 获取指针
template<class T, class X = void, int N = 0>
class Singleton {
public:
    static T* getInstance() {
        static T s;
        return &s;
    }
};

// 获取智能指针
template<class T, class X = void, int N = 0>
class SingletonPtr {
public:
    static std::shared_ptr<T> getInstance() {
        std::shared_ptr<T> s(new T);
        return s;
    }
};
#endif