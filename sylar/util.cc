#include "util.h"
#include <sys/syscall.h>

namespace sylar {

pid_t getPid() {
    return syscall(SYS_gettid);
}

uint32_t getFid() {
    return 0;
}

}