#include "log.h"

namespace sylar {

// 静态方法实现时，前面的static要去掉
const char* LogLevel::ToString(LogLevel::Level level) {
    switch(level) {
#define XX(name) \
        case LogLevel::name: \
            return #name; \
            break; \
        
        XX(DEBUG);
        XX(INFO);
        XX(WARN);
        XX(ERROR);
        XX(FATAL);
#undef XX
        default:
            return "UNKNOWN";
            break;
    }
    return "UNKNOWN";
}

LogEvent::LogEvent(std::shared_ptr<Logger> logger, LogLevel::Level level, const char* file, int32_t line, uint32_t elapse, uint32_t threadId, uint32_t fibraId, uint64_t time) {
    m_file = file;
    m_line = line;
    m_elapse = elapse;
    m_threadId = threadId;
    m_fibraId = fibraId;
    m_time = time;
    m_logger = logger;
    m_level = level;
}

void LogEvent::format(const char* fmt, ...) {
    va_list al;
    va_start(al, fmt);
    format(fmt, al); // 调用下面的format
    va_end(al);
}
void LogEvent::format(const char* fmt, va_list al) {
    char* buf;
    int len = vasprintf(&buf, fmt, al);
    if(len != -1) {
        m_ss << std::string(buf, len); // 使用char* 格式的c风格字符串初始化string
        free(buf);
    }
}

LogEventWrap::LogEventWrap(LogEvent::ptr e) {
    m_event = e;
}
LogEventWrap::~LogEventWrap() {
    if(m_event) {
        m_event->getLogger()->log(m_event->getLevel(), m_event);
    }
}


LogFormatter::LogFormatter(const std::string pattern) : m_pattern(pattern) {
    // std::cout << "init" << std::endl;
    init();
    //std::cout << "init" << std::endl;
}

// %m 消息
class MessageFormatItem : public LogFormatter::FormatItem {
public:
    // 构造函数这里的传参按理说是传fmt，但是并没有用上，即%str{fmt}格式是不曾实现的
    MessageFormatItem(const std::string fmt = "") {}
    void format(std::ostream &os, std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) override {
        os << event->getContent();
    }
};
// %p 日志级别
class LevelFormatItem : public LogFormatter::FormatItem {
public:
    // 构造函数这里的传参按理说是传fmt，但是并没有用上，即%str{fmt}格式是不曾实现的
    LevelFormatItem(const std::string fmt = "") {}
    void format(std::ostream &os, std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) override {
        os << LogLevel::ToString(level);
    }
};

// %c 日志器名称 需要通过log函数传到format之中
class NameFormatItem : public LogFormatter::FormatItem {
public:
    // 构造函数这里的传参按理说是传fmt，但是并没有用上，即%str{fmt}格式是不曾实现的
    NameFormatItem(const std::string fmt = "") {}
    void format(std::ostream &os, std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) override {
        os << logger->getName();
    }
};

// %d 日期时间，后面可跟一对括号指定时间格式，比如%d{%Y-%m-%d %H:%M:%S}，这里的格式字符与C语言strftime一致
class DateTimeFormatItem : public LogFormatter::FormatItem {
public:
    DateTimeFormatItem(const std::string fmt = "%Y-%m-%d %H:%M:%S") : m_format(fmt) {
        if(m_format.empty()) {
            m_format = "%Y-%m-%d %H:%M:%S";
        }
    }
    void format(std::ostream &os, std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) override {
        // 将原始时间转为标准时间
        struct tm tm;
        time_t time = event->getTime();
        localtime_r(&time, &tm);
        char buf[64];
        strftime(buf, sizeof(buf), m_format.c_str(), &tm);
        os << buf;
    }
private:
    std::string m_format;
};
// %r 该日志器创建后的累计运行毫秒数 ElapseFormatItem
class ElapseFormatItem : public LogFormatter::FormatItem {
public:
    // 构造函数这里的传参按理说是传fmt，但是并没有用上，即%str{fmt}格式是不曾实现的
    ElapseFormatItem(const std::string fmt = "") {}
    void format(std::ostream &os, std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) override {
        os << event->getElapse();
    }
};

// %f 文件名 FilenameFormatItem
class FilenameFormatItem : public LogFormatter::FormatItem {
public:
    // 构造函数这里的传参按理说是传fmt，但是并没有用上，即%str{fmt}格式是不曾实现的
    FilenameFormatItem(const std::string fmt = "") {}
    void format(std::ostream &os, std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) override {
        os << event->getFileName();
    }
};

// %l 行号 LineFormatItem
class LineFormatItem : public LogFormatter::FormatItem {
public:
    // 构造函数这里的传参按理说是传fmt，但是并没有用上，即%str{fmt}格式是不曾实现的
    LineFormatItem(const std::string fmt = "") {}
    void format(std::ostream &os, std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) override {
        os << event->getLine();
    }
};

// %t 线程id ThreadIdFormatItem
class ThreadIdFormatItem : public LogFormatter::FormatItem {
public:
    // 构造函数这里的传参按理说是传fmt，但是并没有用上，即%str{fmt}格式是不曾实现的
    ThreadIdFormatItem(const std::string fmt = "") {}
    void format(std::ostream &os, std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) override {
        os << event->getThreadId();
    }
};

// %F 协程id FiberIdFormatItem
class FiberIdFormatItem : public LogFormatter::FormatItem {
public:
    // 构造函数这里的传参按理说是传fmt，但是并没有用上，即%str{fmt}格式是不曾实现的
    FiberIdFormatItem(const std::string fmt = "") {}
    void format(std::ostream &os, std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) override {
        os << event->getFibraId();
    }
};


class StringFormatItem : public LogFormatter::FormatItem {
public:
    StringFormatItem(const std::string& str)
        :m_string(str) {}
    void format(std::ostream& os, std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) override {
        os << m_string;
    }
private:
    std::string m_string;
};

// %N 线程名称
class ThreadNameFormatItem : public LogFormatter::FormatItem {
public:
    ThreadNameFormatItem(const std::string& str = "") {}
    void format(std::ostream& os, std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) override {
        os << event->getThreadName();
    }
};
// %% 百分号
// %T 制表符
class TabFormatItem : public LogFormatter::FormatItem {
public:
    TabFormatItem(const std::string& str = "") {}
    void format(std::ostream& os, std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) override {
        os << "\t";
    }
private:
    std::string m_string;
};

// %n 换行
class NewLineFormatItem : public LogFormatter::FormatItem {
public:
    NewLineFormatItem(const std::string& str = "") {}
    void format(std::ostream& os, std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) override {
        os << std::endl;
    }
};


//用于解析pattern,获得m_items格式项数组
// pattern有三种类型，%格式替代、%str{fmt}：其中str就是单个匹配字符，%%
void LogFormatter::init() {
    //str, format, type
    std::vector<std::tuple<std::string, std::string, int> > vec;
    std::string nstr;
    for(size_t i = 0; i < m_pattern.size(); ++i) {
        // 普通的str
        if(m_pattern[i] != '%') {
            nstr.append(1, m_pattern[i]);
            continue;
        }
        // 此时 m_pattern[i] == '%'
        // %%代表%
        if((i + 1) < m_pattern.size()) {
            if(m_pattern[i + 1] == '%') {
                nstr.append(1, '%');
                continue;
            }
        }

        // 这后面都是%之后的格式化str：%str{fmt}
        size_t n = i + 1;
        int fmt_status = 0;
        size_t fmt_begin = 0;

        std::string str;
        std::string fmt;
        while(n < m_pattern.size()) {

            //相当于遇到空格或者非{}的其他符号了，中断，提取出str
            if(!fmt_status && (!isalpha(m_pattern[n]) && m_pattern[n] != '{'
                    && m_pattern[n] != '}')) {
                str = m_pattern.substr(i + 1, n - i - 1);
                break;
            }
            if(fmt_status == 0) {
                if(m_pattern[n] == '{') {
                    str = m_pattern.substr(i + 1, n - i - 1);
                    //std::cout << "*" << str << std::endl;
                    fmt_status = 1; //解析格式
                    fmt_begin = n;
                    ++n;
                    continue;
                }
            } else if(fmt_status == 1) {
                if(m_pattern[n] == '}') {
                    fmt = m_pattern.substr(fmt_begin + 1, n - fmt_begin - 1);
                    //std::cout << "#" << fmt << std::endl;
                    fmt_status = 0;
                    ++n;
                    break;
                }
            }
            ++n;
            if(n == m_pattern.size()) {
                if(str.empty()) {
                    str = m_pattern.substr(i + 1);
                }
            }
        }

        if(fmt_status == 0) {
            if(!nstr.empty()) {
                vec.push_back(std::make_tuple(nstr, std::string(), 0));
                nstr.clear();
            }
            vec.push_back(std::make_tuple(str, fmt, 1));
            i = n - 1;
        } else if(fmt_status == 1) {
            std::cout << "pattern parse error: " << m_pattern << " - " << m_pattern.substr(i) << std::endl;
            //m_error = true;
            vec.push_back(std::make_tuple("<<pattern_error>>", fmt, 0));
        }
    }

    if(!nstr.empty()) {
        vec.push_back(std::make_tuple(nstr, "", 0));
    }
    // s_format_items是一个map映射，string到一个输入参数string返回值FormatItem::ptr的可调用对象。并用XX宏做一个初始化
    static std::map<std::string, std::function<FormatItem::ptr(const std::string& str)> > s_format_items = {
#define XX(str, C) \
        {#str, [](const std::string& fmt) { return FormatItem::ptr(new C(fmt));}}

        XX(m, MessageFormatItem),           //m:消息
        XX(p, LevelFormatItem),             //p:日志级别
        XX(r, ElapseFormatItem),            //r:累计毫秒数
        XX(c, NameFormatItem),              //c:日志名称
        XX(t, ThreadIdFormatItem),          //t:线程id
        XX(n, NewLineFormatItem),           //n:换行
        XX(d, DateTimeFormatItem),          //d:时间
        XX(f, FilenameFormatItem),          //f:文件名
        XX(l, LineFormatItem),              //l:行号
        XX(T, TabFormatItem),               //T:Tab
        XX(F, FiberIdFormatItem),           //F:协程id
        XX(N, ThreadNameFormatItem),        //N:线程名称
#undef XX
    };

    for(auto& i : vec) {

        // 0相当于无格式的纯字符串
        if(std::get<2>(i) == 0) {
            m_items.push_back(FormatItem::ptr(new StringFormatItem(std::get<0>(i))));
        } else {
            //%str{fmt} 则利用s_format_items做一个匹配回调
            auto it = s_format_items.find(std::get<0>(i));
            // 没有找到打印错误信息
            if(it == s_format_items.end()) {
                m_items.push_back(FormatItem::ptr(new StringFormatItem("<<error_format %" + std::get<0>(i) + ">>")));
                // m_error = true;
            } else {
                // it->second是一个可调用对象，返回Iterm类
                m_items.push_back(it->second(std::get<1>(i)));
            }
        }

        // std::cout << "(" << std::get<0>(i) << ") - (" << std::get<1>(i) << ") - (" << std::get<2>(i) << ")" << std::endl;
    }
    // std::cout << m_items.size() << std::endl;
}

std::string LogFormatter::format(std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) {
    // std::cout << " LogFormatter::format " << std::endl;
    std::stringstream ss;
    for(auto it : m_items) {
        it->format(ss, logger, level, event);
    }
    return ss.str();
}


Logger::Logger(const std::string name) : m_name(name), m_level(LogLevel::DEBUG) {
    // m_formatter.reset(new LogFormatter("%d{%Y-%m-%d %H:%M:%S}%T%t%T%N%T%F%T[%p]%T[%c]%T%f:%l%T%m%n"));
    m_formatter.reset(new LogFormatter("%d{%Y-%m-%d %H:%M:%S}%T%t%T%N%T%F%T[%p]%T[%c]%T%f:%l%T%m%n"));
}
    
void Logger::log(LogLevel::Level level, LogEvent::ptr event) {
    // std::cout << " Logger::log " << std::endl;
    if(level >= m_level) {
        for(auto it : m_appenders) {
            it->log(shared_from_this(), level, event);
        }
    }
}
void Logger::debug(LogEvent::ptr event) {
    log(LogLevel::DEBUG, event);
}
void Logger::info(LogEvent::ptr event) {
    log(LogLevel::INFO, event);
}
void Logger::warn(LogEvent::ptr event) {
    log(LogLevel::WARN, event);
}
void Logger::error(LogEvent::ptr event) {
    log(LogLevel::ERROR, event);
}
void Logger::fatal(LogEvent::ptr event) {
    log(LogLevel::FATAL, event);
}

void Logger::addAppender(LogAppender::ptr appender) {
    if(!appender->getFormatter()) {
        appender->setFormatter(m_formatter);
    }
    m_appenders.push_back(appender);
    // std::cout << " addAppender " << std::endl;
}

//这里del用的是erase方法，故需要使用迭代器
void Logger::delAppender(LogAppender::ptr appender) {
    for(auto it = m_appenders.begin(); it != m_appenders.end(); it++) {
        if(*it == appender) {
            m_appenders.erase(it);
        }
    }
}

void StdoutLogAppender::log(std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) {
    // std::cout << level << std::endl;
    // std::cout << m_level << std::endl;
    if(level >= m_level) {
        std::cout << m_formatter->format(logger, level, event);
    }
}
FileLogAppender::FileLogAppender(const std::string name) : m_filename(name) {
    reopen();
}

bool FileLogAppender::reopen() {
    if(m_filestream) {
        m_filestream.close();
    }
    m_filestream.open(m_filename);
    return !!m_filestream;
}
void FileLogAppender::log(std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) {
    if(level >= m_level) {
        m_filestream << m_formatter->format(logger, level, event);
    }
}

LoggerManager::LoggerManager() {
    m_root.reset(new Logger);
    m_root->addAppender(LogAppender::ptr(new StdoutLogAppender));

    m_loggers[m_root->getName()] = m_root;
}

Logger::ptr LoggerManager::getLogger(const std::string& name) {
    auto it = m_loggers.find(name);
    if(it != m_loggers.end()) {
        return it->second;
    }
    return m_root; // 如果没有则用主日志器输出
    // Logger::ptr logger(new Logger(name));

}


}