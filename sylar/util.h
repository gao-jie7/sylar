#ifndef __SYLAR_UTIL_H
#define __SYLAR_UTIL_H
#include <stdint.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syscall.h>
namespace sylar {

pid_t getPid();
uint32_t getFid();

}


#endif