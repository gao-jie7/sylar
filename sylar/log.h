#ifndef __SYLAR_LOG_H
#define __SYLAR_LOG_H

#include <memory>
#include <string>
#include <stdint.h>
#include <list>
#include <fstream>
#include <sstream>
#include <ostream>
#include <vector>
#include <iostream>
#include <map>
#include <functional>
#include <stdarg.h>
#include "singleton.h"

// 流式日志输出
#define SYLAR_LOG_LEVEL(logger, level) \
    if(logger->getLevel() <= level) \
        sylar::LogEventWrap(sylar::LogEvent::ptr(new sylar::LogEvent(logger, level, \
                        __FILE__, __LINE__, 0, sylar::getPid(),\
                sylar::getFid(), time(0)))).getSS()

#define SYLAR_LOG_DEBUG(logger) SYLAR_LOG_LEVEL(logger, sylar::LogLevel::DEBUG)
#define SYLAR_LOG_INFO(logger) SYLAR_LOG_LEVEL(logger, sylar::LogLevel::INFO)
#define SYLAR_LOG_WARN(logger) SYLAR_LOG_LEVEL(logger, sylar::LogLevel::WARN)
#define SYLAR_LOG_ERROR(logger) SYLAR_LOG_LEVEL(logger, sylar::LogLevel::ERROR)
#define SYLAR_LOG_FATAL(logger) SYLAR_LOG_LEVEL(logger, sylar::LogLevel::FATAL)

// 格式化日志输出
#define SYLAR_LOG_FMT_LEVEL(logger, level, fmt, ...) \
    if(logger->getLevel() <= level) \
        sylar::LogEventWrap(sylar::LogEvent::ptr(new sylar::LogEvent(logger, level, \
                        __FILE__, __LINE__, 0, sylar::getPid(),\
                sylar::getFid(), time(0)))).getEvent()->format(fmt, __VA_ARGS__)

#define SYLAR_LOG_FMT_DEBUG(logger, fmt, ...) SYLAR_LOG_FMT_LEVEL(logger, sylar::LogLevel::DEBUG, fmt, __VA_ARGS__)
#define SYLAR_LOG_FMT_INFO(logger, fmt, ...) SYLAR_LOG_FMT_LEVEL(logger, sylar::LogLevel::INFO, fmt, __VA_ARGS__)
#define SYLAR_LOG_FMT_WARN(logger, fmt, ...) SYLAR_LOG_FMT_LEVEL(logger, sylar::LogLevel::WARN, fmt, __VA_ARGS__)
#define SYLAR_LOG_FMT_ERROR(logger, fmt, ...) SYLAR_LOG_FMT_LEVEL(logger, sylar::LogLevel::ERROR, fmt, __VA_ARGS__)
#define SYLAR_LOG_FMT_FATAL(logger, fmt, ...) SYLAR_LOG_FMT_LEVEL(logger, sylar::LogLevel::FATAL, fmt, __VA_ARGS__)

namespace sylar {
class Logger;
// 日志等级
class LogLevel {
public:
    enum Level {
        UNKNOWN = 0,
        DEBUG = 1,  // 每个appender的默认等级
        INFO = 2,
        WARN = 3,
        ERROR = 4,
        FATAL = 5
    };

    static const char* ToString(LogLevel::Level level);
};

class LogEvent {
public:
    typedef std::shared_ptr<LogEvent> ptr;
    LogEvent(std::shared_ptr<Logger> logger, LogLevel::Level level, const char* file, int32_t line, uint32_t elapse, uint32_t threadId, uint32_t fibraId, uint64_t time);

    std::string getContent() const { return m_ss.str();}
    const char* getFileName() const { return m_file;}
    int32_t getLine() const { return m_line;}
    uint32_t getElapse() const { return m_elapse;}
    uint32_t getThreadId() const { return m_threadId;}
    uint32_t getFibraId() const { return m_fibraId;}
    uint64_t getTime() const { return m_time;}
    std::string getThreadName() const { return "getThreadName";}

    // 获取输入流用于写日志内容
    std::stringstream& getSS() { return m_ss;}  // 流必须是引用类型

    // 格式化输入流写日志内容
    void format(const char* fmt, ...);
    void format(const char* fmt, va_list al);

    std::shared_ptr<Logger> getLogger() const { return m_logger;}
    LogLevel::Level getLevel() const { return m_level;}


private:
    const char* m_file = nullptr; //文件名
    int32_t m_line = 0;           //行号
    uint32_t m_elapse = 0;        //从开始到现在的执行时间
    uint32_t m_threadId = 0;      //线程ID
    uint32_t m_fibraId = 0;       //协程ID
    uint64_t m_time = 0;          //时间戳 传time(0)获取未经转换的原始时间
    std::stringstream m_ss;

    std::shared_ptr<Logger> m_logger;
    LogLevel::Level m_level;
};


class LogEventWrap {
public:
    LogEventWrap(LogEvent::ptr e);
    ~LogEventWrap(); //在析构函数里执行log操作

    LogEvent::ptr getEvent() const { return m_event;}
    std::stringstream& getSS() { return m_event->getSS();}
    
private:
    LogEvent::ptr m_event;
};



class LogFormatter {
public:
    typedef std::shared_ptr<LogFormatter> ptr;
    
    LogFormatter(const std::string pattern);

    //根据格式输出日志内容,内部实现则是遍历m_items调用其format实现的
    std::string format(std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event);

    //用于解析pattern,获得m_items格式项数组
    void init();

    std::string getPattern() const { return m_pattern;}

public:

    //代表具体某个通配，被继承
    class FormatItem {
    public:
        typedef std::shared_ptr<FormatItem> ptr;
        virtual ~FormatItem() {}
        virtual void format(std::ostream &os, std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) = 0;
    };
private:
    std::string m_pattern;
    std::vector<FormatItem::ptr> m_items;
};


class LogAppender {
public:
    typedef std::shared_ptr<LogAppender> ptr;

    //因为要被继承，故定义虚析构函数,虚析构函数就是要加virtual关键字的
    virtual ~LogAppender() {}

    //log函数用以打印日志，定义为纯虚函数被子类实现，子类结尾加个override
    virtual void log(std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) = 0;

    LogFormatter::ptr getFormatter() const { return m_formatter;}
    void setFormatter(LogFormatter::ptr formatter) { m_formatter = formatter;};

    void setLevel(LogLevel::Level level) { m_level = level;}
    LogLevel::Level getLevel() const { return m_level;}

//定义成protected为了使子类能够访问到该父类成员
protected:
    LogLevel::Level m_level = LogLevel::DEBUG;
    LogFormatter::ptr m_formatter;
};

class Logger : public std::enable_shared_from_this<Logger> {
public:
    typedef std::shared_ptr<Logger> ptr;
    Logger(const std::string name = "root");
    void log(LogLevel::Level level, LogEvent::ptr event);
    void debug(LogEvent::ptr event);
    void info(LogEvent::ptr event);
    void warn(LogEvent::ptr event);
    void error(LogEvent::ptr event);
    void fatal(LogEvent::ptr event);

    LogLevel::Level getLevel() const { return m_level;}
    void setLevel(LogLevel::Level level) { m_level = level;}

    void addAppender(LogAppender::ptr appender);
    void delAppender(LogAppender::ptr appender);

    std::string getName() const { return m_name;}
private:
    std::string m_name;
    LogLevel::Level m_level;                 //只有大于等于m_level级别的日志才会被输出
    std::list<LogAppender::ptr> m_appenders; //Logger管理多个appender
    LogFormatter::ptr m_formatter;
};

class StdoutLogAppender : public LogAppender {
public:
    typedef std::shared_ptr<StdoutLogAppender> ptr;
    void log(std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) override;
private:

};


class FileLogAppender :  public LogAppender {
public:
    typedef std::shared_ptr<FileLogAppender> ptr;
    FileLogAppender(const std::string name);
    void log(std::shared_ptr<Logger> logger, LogLevel::Level level, LogEvent::ptr event) override;
    bool reopen();
private:
    std::string m_filename;
    std::ofstream m_filestream;
};

class LoggerManager {
public:
    LoggerManager();
    Logger::ptr getLogger(const std::string& name);
    Logger::ptr getRoot() const { return m_root;}

    // init();

private:
    Logger::ptr m_root;
    std::map<std::string, Logger::ptr> m_loggers;
};

typedef SingletonPtr<LoggerManager> LogMgr; // 用来定义单例类对象保证唯一

}
#endif